package lot3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class Game extends Thread {
	private final Socket s;
	private String nom;
	private boolean jeSuisGagnant;

	public Game(Socket soc, String n) {
		super();
		this.s = soc;
		this.nom = n;
	}

	public void run() {
		try {
			// Envoi demande du nom
			OutputStream os = s.getOutputStream();
			os.write("\r\nSaisir votre nom : ".getBytes());

			// Lecture du nom
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			nom = in.readLine();

			// Envoi de la question
			String str = "\r\nBienvenue " + nom + ", choisissez un nombre entre 0 et 100 : ";
			os = s.getOutputStream();
			os.write(str.getBytes());
			int nbre = 0;

			// Lecture du nombre
			int cpt = 4;
			while (!ServeurMulti.gagnant && cpt > 0) {
				in = new BufferedReader(new InputStreamReader(s.getInputStream()));
				str = in.readLine();
				nbre = Integer.parseInt(str);
				if (nbre < ServeurMulti.x) {
					str = "\r\n-\n";

				} else if (nbre > ServeurMulti.x) {
					str = "\r\n+\n";

				} else if (nbre == ServeurMulti.x) {
					str = "\r\nBravo " + nom + "\n";
					ServeurMulti.gagnant = true;
					this.jeSuisGagnant = true;
				}
				os.write(str.getBytes());
				cpt--;
			}
			if (cpt == 0) {
				str = "\r\nPerdu " + nom;
				os.write(str.getBytes());
			} else if (ServeurMulti.gagnant && !this.jeSuisGagnant) {
				str = "\r\nPerdu " + nom;
				os.write(str.getBytes());
			}
			Thread.sleep(1000);
			os.write("\r\nFin de connexion Serveur\n".getBytes());
			System.out.println("Fin de connexion");

			this.s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
