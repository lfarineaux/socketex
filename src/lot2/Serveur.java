package lot2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Random;

public class Serveur {

	public static void main(String[] args) throws IOException, InterruptedException {
		Random r = new Random();

		do 	{
			ServerSocket s = new ServerSocket (8101);
//		s.setSoTimeout(20000);
			int x = r.nextInt(101);

			System.out.println("Serveur");

			// Attente Connexion
			Socket socServ = s.accept();
			System.out.println("Connexion serveur Ok");

			// Envoi connexion r�ussie
			OutputStream os = socServ.getOutputStream ();
			os.write ("Connexion client Ok\n".getBytes());
			//			PrintWriter out=new PrintWriter(new OutputStreamWriter(socServ.getOutputStream()));
			//			out.println("Connexion client Ok");
			//			out.flush();

			// Envoi demande du nom
			os = socServ.getOutputStream ();
			os.write ("\r\nSaisir votre nom : ".getBytes());

			// Lecture du nom
			InputStream is = socServ.getInputStream ();
			BufferedReader in = new BufferedReader(new InputStreamReader(socServ.getInputStream()));
			String nom = in.readLine();
			//			byte[] in = new byte[70];
			//			is.read(in);
			//			String nom = new String (in);
			//			nom=nom.trim();

			// Envoi de la question
			String str = "Bienvenue "+nom+", choisissez un nombre entre 0 et 100 : ";
			os= socServ.getOutputStream ();
			os.write (str.getBytes());

			// Lecture du nombre
			boolean fin=false;
			int cpt=4;
			while (!fin && cpt>0) {
				in = new BufferedReader(new InputStreamReader(socServ.getInputStream()));
				str = in.readLine();
				int nbre=Integer.parseInt(str);
				//				int nbre=is.read();
				if (nbre==x) {
					str="\rBravo "+nom+"\n";
					fin=true;
				}
				if (nbre<x) {
					str="\r\n-\n";
				}
				if (nbre>x) {
					str="\r\n+\n";
				}
				if (cpt==1) {
					str="\r\nPerdu "+nom;
					fin=true;
				}
				os.write (str.getBytes());
				cpt--;
			}

			Thread.sleep(1000);
			os.write ("\r\nFin de connexion Serveur\n".getBytes());
			System.out.println("Fin de connexion");
			//			out.close();
			socServ.close();
			s.close();
		}
		while (true) ;
	}
}