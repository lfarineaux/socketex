package lot4;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class ServeurMulti extends Thread {

	public static String nom;
	
	public static int nbreClient;
	public static int x;
	
	public static boolean gagnant=false;

	Random r= new Random();

	public void run() {
		System.out.println("Serveur");
		try {
			x=r.nextInt(101);
			ServerSocket servmulti = new ServerSocket(8101);	
			while (true) {
				// Attente Connexion
				Socket s=servmulti.accept();
				nbreClient++;
				
				System.out.println("Connexion serveur Ok");
				
				// Envoi connexion r�ussie
				OutputStream os = s.getOutputStream ();
				if (nbreClient>4) {
					os.write ("Trop tard\n".getBytes());
					s.close();
				}
				os.write ("Connexion client Ok".getBytes());
				
				new Game(s,nom).start();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}
	public static void main(String[] args) {
		new ServeurMulti().start();
	}
}
