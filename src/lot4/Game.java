package lot4;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import lot3.ServeurMulti;

public class Game extends Thread {
	private final Socket s;
	private String nom;

	public Game (Socket soc,String n) {
		super();
		this.s = soc;
		this.nom=n;
	}

	public void run () {
		try {
			// Envoi demande du nom
			OutputStream os = s.getOutputStream ();
			os.write ("\r\nSaisir votre nom : ".getBytes());

			// Lecture du nom
			InputStream is = s.getInputStream ();
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String nom = in.readLine();

			// Envoi de la question
			String str = "\r\nBienvenue "+nom+", choisissez un nombre entre 0 et 100 : ";
			os= s.getOutputStream ();
			os.write (str.getBytes());
			int nbre=0;

			// Lecture du nombre
			int cpt=4;
			ServeurMulti.fin=false;
			while (!ServeurMulti.fin && cpt>0) {
				in = new BufferedReader(new InputStreamReader(s.getInputStream()));
				str = in.readLine();
				nbre=Integer.parseInt(str);
				//				int nbre=is.read();
				if (!ServeurMulti.fin && cpt>0) {
					if (nbre<ServeurMulti.x) {
						str="\r\n-\n";
					}
					if (nbre>ServeurMulti.x) {
						str="\r\n+\n";
					}
					if (nbre==ServeurMulti.x) {
						str="\r\nBravo "+nom+"\n";
						ServeurMulti.fin=true;
					}
				}
				if (cpt==1){
					str="\r\nPerdu "+nom;
					ServeurMulti.fin=true;
				}
				cpt--;
				os.write (str.getBytes());
			}
			Thread.sleep(1000);
			os.write ("\r\nFin de connexion Serveur\n".getBytes());
			System.out.println("Fin de connexion");
			this.s.close();
			ServeurMulti.nbreClient--;
		}
		catch  (Exception e){
			e.printStackTrace();
		}
	}
}
